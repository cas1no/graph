/* all:
gcc -std=c11 graph.c
./a.out | gnuplot -p */

#include <stdio.h>

#define N 2000
//----------------------------------------------------------------------
static void plot(int *a, size_t len) {
    fprintf(stdout,
        "set datafile separator ','\n"
        //"set terminal png size 1024,768\n"
        "set title '%s'\n"
        "set xlabel '%s'\n"
        "set ylabel '%s'\n"
        "set key left top\n"
        "set grid\n",
        "Fly straight, damn it!", "x", "y"
    );
    fprintf(stdout, "plot '-' with points title 'samples' pt 7\n");
    for (size_t i = 0; i < len; i++) {
        fprintf(stdout, "%zu,%d\n", i, a[i]);
    }
}
//----------------------------------------------------------------------
int gcd(int a, int b) {
	if (b == 0)
		return a;
	else
		return gcd(b, a % b);
}
//----------------------------------------------------------------------
int main(void) {
	int a[N] = {0};

	a[0] = 1;
	a[1] = 1;

	for (int i = 2; i < N; i++) {
		int gcdNumber = gcd(a[i-1], i);
		
		if (gcdNumber == 1) 			// numbers are coprime
			a[i] = a[i-1] + i + 1;
		else
			a[i] = a[i-1]/gcd(a[i-1], i);
			
	}
	
	plot(a, N);

	return 0;
}
